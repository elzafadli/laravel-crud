<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
 
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan/create', 'PertanyaanController@store');
Route::post('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');  
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@edit');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@show'); 
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy')->name('pertanyaan.destroy');

Route::get('/jawaban/{pertanyaan_id}', 'JawabanController@show');
Route::post('/jawaban/{pertanyaan_id}', 'JawabanController@store');
// Route::post('/welcome', 'AuthController@create');
// Route::get('/', 'AdminController@index');
// Route::get('/data-tables', 'AdminController@datatable');

