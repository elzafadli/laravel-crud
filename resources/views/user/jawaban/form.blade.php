@extends('../admin.app')

@section('content')
<form method="post" action="{{ url('jawaban/'.$data->id) }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="pertanyaan_id" value={{$data->id}}>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Pertanyaan</label>
                <input type="text" disabled class="form-control" required value="{{$data->judul}} ?">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Jawaban</label>
                <input type="text" name="isi" class="form-control" required placeholder="Enter Isi">
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection