@extends('../admin.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with default features</h3>
        <a href="{{url('pertanyaan/create')}}" class="btn btn-primary pull-right float-right">Create</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Jawaban</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->isi}}</td>
                    <td>
                        <ul>

                            @foreach ($item->jawaban as $r)
                            <li>{{$r->isi}}</li>
                            @endforeach

                        </ul>
                    </td>
                    <td>
                        <div class="box-footer">
                            <button type="button" class="btn btn-success pull-right"
                                onclick="viewQuestion({{$item->id}})">View</button>
                            <button type="button" class="btn btn-info pull-right"
                                onclick="addQuestion({{$item->id}})">Add</button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });

    function addQuestion(id) {
        url = '{{url('jawaban')}}' + '/' + id,
        window.open(url, '_blank');
    }

    function viewQuestion(id) {
        url = '{{url('pertanyaan')}}' + '/' + id + '/edit',
        window.open(url, '_blank');
    }

</script>
@endpush
