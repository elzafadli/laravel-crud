@extends('../admin.app')

@section('content')
    @isset($data->id)
        <input type="hidden" value="{{$data->id}}">   
    @endisset
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Judul</label>
            <input type="text" name="judul" class="form-control" required placeholder="Enter Judul" {{isset($data->judul) ? 'disabled' : ''}} value="{{isset($data->judul) ? $data->judul : '' }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Isi</label>
                <input type="text" name="isi" class="form-control" required placeholder="Enter Isi" {{isset($data->isi) ? 'disabled' : ''}} value="{{isset($data->isi) ? $data->isi : ''}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Dibuat</label>
                <input type="text" name="isi" class="form-control" {{isset($data->created_at) ? 'disabled' : ''}} value="{{isset($data->created_at) ? $data->created_at : ''}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Diperbaharui</label>
                <input type="text" name="isi" class="form-control" {{isset($data->updated_at) ? 'disabled' : ''}} value="{{isset($data->updated_at) ? $data->updated_at : ''}}">
            </div>
        </div>
    </div>
    
    @isset($data->id)
    <hr>
    <h5>List Jawaban :</h5>
    <ul>
        @foreach ($data->jawaban as $r)
            <li>{{$r->isi}}</li>
        @endforeach
    </ul>
    @endisset

    <div class="box-footer">
        <div class="row">
            <div class="col-md-12" style="display: flex">
                <a href="{{url('/pertanyaan/'.$data->id)}}" class="btn btn-primary" style="margin-right: 10px">Edit</a>

                <form action="{{ route('pertanyaan.destroy',$data->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
@endsection