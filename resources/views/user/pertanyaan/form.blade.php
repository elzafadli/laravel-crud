@extends('../admin.app')

@section('content')
<form method="post" action="{{ isset($data->id) ?  url('pertanyaan/'.$data->id) : url('pertanyaan/create') }}">
    @csrf
    @isset($data->id)
        <input type="hidden" name="id" value="{{$data->id}}">   
    @endisset
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Judul</label>
            <input type="text" name="judul" class="form-control" required placeholder="Enter Judul" value="{{isset($data->judul) ? $data->judul : '' }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Isi</label>
                <input type="text" name="isi" class="form-control" required placeholder="Enter Isi" value="{{isset($data->isi) ? $data->isi : ''}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Dibuat</label>
                <input type="text" name="isi" class="form-control" disabled value="{{isset($data->created_at) ? $data->created_at : ''}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Diperbaharui</label>
                <input type="text" name="isi" class="form-control" disabled value="{{isset($data->updated_at) ? $data->updated_at : ''}}">
            </div>
        </div>
    </div>

    <div class="box-footer">
        @if(isset($data->id))
            <button type="submit" class="btn btn-primary">Edit</button>
        @else
            <button type="submit" class="btn btn-primary">Submit</button>
        @endif
    </div>
</form>
@endsection